#!/usr/bin/perl

=comment

Copyright (c) 2003-2010, Andrew Dunstan

See accompanying License file for license details

=cut 

use strict;
use DBI;
use Template;
use CGI;

use vars qw($dbhost $dbname $dbuser $dbpass $dbport $template_dir);

use FindBin qw($RealBin);
require "$RealBin/../BuildFarmWeb.pl";

my $query = new CGI;
my @members = $query->multi_param('member');
map { s/[^a-zA-Z0-9_ -]//g; } @members;

my $dsn="dbi:Pg:dbname=$dbname";
$dsn .= ";host=$dbhost" if $dbhost;
$dsn .= ";port=$dbport" if $dbport;


my $sort_clause = "";
my $sortby = $query->param('sortby') || 'nosort';
if ($sortby eq 'name')
{
	$sort_clause = 'lower(sysname),';
}
elsif ($sortby eq 'os')
{
	$sort_clause = 'lower(operating_system), os_version desc,'; 
}
elsif ($sortby eq 'compiler')
{
	$sort_clause = "lower(compiler), compiler_version,";
}

my $db = DBI->connect($dsn,$dbuser,$dbpass,{pg_expand_array => 0}) 
    or die("$dsn,$dbuser,$dbpass,$!");

my $statement =<<EOS;


  SELECT timezone('GMT'::text, now())::timestamp(0) without time zone - b.snapshot AS when_ago,
	b.*,
	s.log_text
  FROM dashboard_mat b LEFT JOIN build_status_log s
  ON    b.sysname = s.sysname
    AND b.snapshot = s.snapshot
    AND s.log_stage = 'test-results.log'
  ORDER BY b.branch = 'master' desc,
        b.branch desc, $sort_clause 
        b.snapshot desc

EOS
;

my $statrows=[];
my $sth=$db->prepare($statement);
$sth->execute;

# walk the set of result rows from the SQL query above
while (my $row = $sth->fetchrow_hashref)
{
    next if (@members && ! grep {$_ eq $row->{sysname} } @members);

    $row->{build_flags}  =~ s/^\{(.*)\}$/$1/;
    $row->{build_flags}  =~ s/,/ /g;
    $row->{build_flags}  =~ s/_PC\b//g;
    $row->{build_flags} = lc($row->{build_flags});

    if (defined($row->{log_text}))
      {		# convert to a hash, find ranges, output list of ranges
      my $h = { split /\s+/, $row->{log_text} };
      my ($i, $start, $last);
      my @ranges;

      foreach my $k (sort {$a<=>$b} keys %$h)
	{
	if (defined $start)
	  {
	  if ($h->{$k} ne $h->{$start} || $k != $last + 1)
	    {
	    # The result (skiped, Passed, Failed) for this testcase number
	    # is different to the start one of the range,
	    # or the range became non-contiguous.
	    # Add text for the range to list of ranges
	    # (these three elements get used by "status.tt" BLOCK colourbar)
	    # Reset the start and the count, for a new range.

	    push @ranges, sprintf("%s %s %s", $h->{$start},  $start,  $i);
	    $start = $k;
	    $i = 1;
	    }
	  else
	    {
	    # bump the size of this range
	    $i++;
	    }
	  }
	else
	  {
	  # First ever range for this row
	  $start = $k;
	  $i = 1;
	  }
	$last = $k;
	}
      if (defined $start)
	{
	# close out the final range
        push @ranges, sprintf("%s %s %s", $h->{$start},  $start,  $i);
	}

      $row->{log_text} = \@ranges;
      }
    push(@$statrows,$row);
}
$sth->finish;


$db->disconnect;


my $template_opts = { INCLUDE_PATH => $template_dir };
my $template = new Template($template_opts);

print "Content-Type: text/html\n\n";

$template->process('status.tt',
		{statrows=>$statrows});

exit;

