#!/usr/bin/perl

=comment

Copyright (c) 2003-2010, Andrew Dunstan

See accompanying License file for license details

=cut 

use strict;
use Template;

use vars qw( $template_dir );

use FindBin qw($RealBin);
require "$RealBin/../BuildFarmWeb.pl";

my $template_opts = { INCLUDE_PATH => $template_dir };
my $template = new Template($template_opts);

print "Content-Type: text/html\n\n";

my $argop = '+';
$argop = '-' if time % 2;
my $arg1 = int(rand(100));
$arg1 += 11 if $arg1 < 12;
my $arg2 = int(rand(10) + 1);

$template->process('register-form.tt', {arg1 => $arg1, arg2 => $arg2, argop => $argop});
