#!/usr/bin/perl
# Update the compiler name for a system

use strict;
use warnings;
use DBI;
use Data::Dumper;

die "Must pass current sysname and new compiler name\n" unless scalar @ARGV == 2;

use vars qw($dbhost $dbname $dbuser $dbpass $dbport
            $user_list_format
);

use FindBin qw($RealBin);
require "$RealBin/../BuildFarmWeb.pl";

die "no dbname" unless $dbname;
die "no dbuser" unless $dbuser;

my $dsn="dbi:Pg:dbname=$dbname";
$dsn .= ";host=$dbhost" if $dbhost;
$dsn .= ";port=$dbport" if $dbport;

my $db = DBI->connect($dsn,$dbuser,$dbpass);

die $DBI::errstr unless $db;

my $sth_up = $db->prepare(q[
       UPDATE buildsystems AS b
       SET compiler = ?
       WHERE name = ?
      ]);
$sth_up->execute($ARGV[1],$ARGV[0]);

my $sth = $db->prepare(q[ 
       SELECT name, status, compiler, compiler_version, sys_owner, owner_email
       FROM buildsystems AS b
       ORDER BY name ASC
      ]);
$sth->execute();

printf $user_list_format,
       "SysName", "Status", "Owner", "Email", "Compiler", "Version";
while (my $row = $sth->fetchrow_hashref)
{
  printf $user_list_format,
                  $row->{name}, $row->{status}, $row->{sys_owner},
                  $row->{owner_email}, $row->{compiler}, $row->{compiler_version};
}
$db->disconnect();
