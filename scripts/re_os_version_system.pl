#!/usr/bin/perl
# Update the OS version for a system

use strict;
use warnings;
use DBI;
use Data::Dumper;

die "Must pass current sysnames and new version-string\n" unless scalar @ARGV == 2;

use vars qw($dbhost $dbname $dbuser $dbpass $dbport
            $user_list_format
);

use FindBin qw($RealBin);
require "$RealBin/../BuildFarmWeb.pl";

die "no dbname" unless $dbname;
die "no dbuser" unless $dbuser;

my $dsn="dbi:Pg:dbname=$dbname";
$dsn .= ";host=$dbhost" if $dbhost;
$dsn .= ";port=$dbport" if $dbport;

my $db = DBI->connect($dsn,$dbuser,$dbpass);

die $DBI::errstr unless $db;

my $sth_up = $db->prepare(q[
       UPDATE buildsystems AS b
       SET os_version = ?
       WHERE name = ?
      ]);
$sth_up->execute($ARGV[1],$ARGV[0]);

my $sth = $db->prepare(q[ 
       SELECT name, status, operating_system, os_version, sys_owner, owner_email
       FROM buildsystems AS b
       ORDER BY name ASC
      ]);
$sth->execute();

printf $user_list_format,
       "SysName", "Status", "Owner", "Email", "Distro", "Version";
while (my $row = $sth->fetchrow_hashref)
{
  printf $user_list_format,
                  $row->{name}, $row->{status}, $row->{sys_owner},
                  $row->{owner_email}, $row->{operating_system},
                  $row->{os_version};
}
$db->disconnect();
